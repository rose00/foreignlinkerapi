# Preconditions

Visual Studio 2019 Community Edition
JDK 16 Early Eaccess


# Build

```batch
vcvars64
mvn clean package
```

# Execute

```batch
vcvars64
java --add-modules jdk.incubator.foreign -Dforeign.restricted=permit "-Djava.library.path=%PATH%;driver\target" -jar driver\target\driver-0-SNAPSHOT.jar
```
