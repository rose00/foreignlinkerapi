package eu.headcrashing;

import org.openjdk.jmh.annotations.Benchmark;

public class MyDriver {

    public static void main(String[] args) throws Throwable {
		org.openjdk.jmh.Main.main(args);
    }

	@Benchmark
	public void jre() {
		MyClassJRE.myMethod("hello");
	}

	@Benchmark
	public void jna() {
		MyClassJNA.myMethod("hello");
	}

	@Benchmark
	public void jni() {
		MyClassJNI.myMethod("hello");
	}

	@Benchmark
	public void fla() {
		MyClassFLA.myMethod("hello");
	}

}
